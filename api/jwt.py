from datetime import datetime
import jwt
from django.conf import settings
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication, get_authorization_header
from api.models import Users


class JWTAuthentication(BaseAuthentication):
    def authenticate(self, request):
        auth_header = get_authorization_header(request)
        auth_data = auth_header.decode('utf-8')
        auth_token = auth_data.split(' ')
        try:
            token = auth_token[1]
        except:
            raise exceptions.AuthenticationFailed('Token not valid')
        if len(auth_token) != 2:
            raise exceptions.AuthenticationFailed('Token not valid')
        try:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms='HS256')
            email = payload['email']
            user = Users.objects.get(email=email)
            return user, auth_token
        except jwt.ExpiredSignatureError as ex:
            raise exceptions.AuthenticationFailed('Token is expired')
        except jwt.DecodeError as ex:
            raise exceptions.AuthenticationFailed('Token is invalid')
