from rest_framework import serializers

from api.models import Office, Staff


class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ['id', 'name', 'title']


class StaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = '__all__'


class StaffUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = ['first_name', 'last_name', 'third_name', 'gender', 'joined_date', 'order_number', 'permanent_address',
                  'phone_number', 'date_of_birth', 'nationality', 'citizenship', 'health_status', 'occupation',
                  'education_level', 'highest_degree_earned', 'place_of_last_education']


class GetStaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = ['id', 'first_name', 'last_name', 'third_name', 'joined_date']
