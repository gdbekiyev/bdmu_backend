from rest_framework import serializers
from api.models import Provincial, ProvincialProfile, Users


class ProvincialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provincial
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['first_name', 'last_name', 'email', 'third_name', 'username']


class ProvincialUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProvincialProfile
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['user'] = UserSerializer(instance.user).data
        response['provincial_fk'] = ProvincialSerializer(instance.provincial_fk).data
        return response
