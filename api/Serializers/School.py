from rest_framework import serializers

from api.Serializers.District import DistrictSerializer
from api.models import SchoolOffice, Office


class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = '__all__'


class OfficeGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ['id', 'name', 'title', 'phone_number', 'address']


class SchoolGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = SchoolOffice
        fields = ['office', 'id']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['office'] = OfficeGetSerializer(instance.office).data
        return response


class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = SchoolOffice
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['office'] = OfficeSerializer(instance.office).data
        response['district'] = DistrictSerializer(instance.district).data
        return response
