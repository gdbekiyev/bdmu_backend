from rest_framework import serializers

from api.models import StaffType


class StaffTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = StaffType
        fields = '__all__'
