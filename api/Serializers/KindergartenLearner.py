from rest_framework import serializers

from api.Serializers.GroupClassSerializers import GroupClassSerializer
from api.models import Users, KindergartenProfile, Learner


class LearnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Learner
        fields = '__all__'


class KindergartenLearnerUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = KindergartenProfile
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['learner'] = LearnerSerializer(instance.learner).data
        return response


class GetLearnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Learner
        fields = ['name', 'surname', 'patronymic', 'date_of_birth', 'group']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['group'] = GroupClassSerializer(instance.group).data
        return response


class GetKindergartenLearnerUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = KindergartenProfile
        fields = ['id', 'learner']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['learner'] = GetLearnerSerializer(instance.learner).data
        return response
