from rest_framework import serializers

from api.models import GroupClass, Office, Staff


class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ['id', 'name', 'title']


class GroupStaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = ['id', 'first_name', 'last_name', 'third_name']


class GroupClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupClass
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['staff_id'] = GroupStaffSerializer(instance.staff_id).data
        return response
