from rest_framework import serializers

from api.Serializers.District import ProvincialSerializer
from api.models import HigherEducationInstituteOffice, Office


class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = '__all__'


class OfficeGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ['id', 'name', 'title', 'phone_number', 'address']


class HigherEducationInstituteGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = HigherEducationInstituteOffice
        fields = ['office', 'id']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['office'] = OfficeGetSerializer(instance.office).data
        return response


class HigherEducationInstituteSerializer(serializers.ModelSerializer):
    class Meta:
        model = HigherEducationInstituteOffice
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['office'] = OfficeSerializer(instance.office).data
        response['province'] = ProvincialSerializer(instance.province).data
        return response
