from rest_framework import serializers

from api.models import DirectorProfile, Users, Office


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['id', 'first_name', 'last_name', 'third_name', 'username', 'email']


class OfficeSerializers(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ['id', 'name', 'title']


class DirectorSerializer(serializers.ModelSerializer):
    class Meta:
        model = DirectorProfile
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['user'] = UserSerializers(instance.user).data
        response['office_fk'] = OfficeSerializers(instance.office_fk).data
        return response


