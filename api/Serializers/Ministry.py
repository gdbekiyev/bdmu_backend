from rest_framework import serializers

from api.models import Users


class MinistrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['id', 'first_name', 'last_name', 'third_name', 'password', 'email', 'username']


class MinistryUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['id', 'first_name', 'last_name', 'third_name', 'email', 'username']
