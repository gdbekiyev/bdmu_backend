from rest_framework import serializers

from api.Serializers.District import ProvincialSerializer
from api.models import SecondaryProfessionalSchoolOffice, Office


class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = '__all__'


class OfficeGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ['id', 'name', 'title', 'phone_number', 'address']


class SecondaryProfessionalSchoolGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = SecondaryProfessionalSchoolOffice
        fields = ['office', 'id']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['office'] = OfficeGetSerializer(instance.office).data
        return response


class SecondaryProfessionalSchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = SecondaryProfessionalSchoolOffice
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['office'] = OfficeSerializer(instance.office).data
        response['province'] = ProvincialSerializer(instance.province).data
        return response
