from rest_framework import serializers
from api.models import District, DistrictProfile, Users, Provincial


class ProvincialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provincial
        fields = '__all__'


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = ['id', 'name', 'title','provincial_fk']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['provincial_fk'] = ProvincialSerializer(instance.provincial_fk).data
        return response


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['first_name', 'last_name', 'email', 'third_name', 'username']


class DistrictUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = DistrictProfile
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['user'] = UserSerializer(instance.user).data
        response['district_fk'] = DistrictSerializer(instance.district_fk).data
        return response
