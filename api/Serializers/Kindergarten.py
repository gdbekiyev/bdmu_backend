from rest_framework import serializers

from api.Serializers.District import DistrictSerializer
from api.models import KindergartenOffice, Office


class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = '__all__'


class OfficeGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ['id', 'name', 'title', 'phone_number', 'address']


class KindergartenGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = KindergartenOffice
        fields = ['office', 'id']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['office'] = OfficeGetSerializer(instance.office).data
        return response


class KindergartenSerializer(serializers.ModelSerializer):
    class Meta:
        model = KindergartenOffice
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['office'] = OfficeSerializer(instance.office).data
        response['district'] = DistrictSerializer(instance.district).data
        return response

