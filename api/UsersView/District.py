from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from api.Serializers.District import DistrictSerializer, DistrictUserSerializer
from api.Serializers.Provincial import ProvincialUserSerializer
from api.models import District, DistrictProfile, Users


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class DistrictViewSet(ViewSet):
    @staticmethod
    def list(request):
        models = District.objects.all()
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = DistrictSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def retrieve(request, pk=None):
        model = District.objects.get(id=pk)
        serializer = DistrictSerializer(model, context={'request': request})
        return Response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            serializer = DistrictSerializer(data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            model = get_object_or_404(District.objects.all(), pk=pk)
            serializer = DistrictSerializer(model, data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat Üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        print(pk)
        model = get_object_or_404(District.objects.all(), id=pk)
        print(model)
        model.delete()
        return Response({'error': False, 'message': 'Maglumat Pozuldy!'})


class GetDistrictUserViewSet(ViewSet):
    @staticmethod
    def list(request):
        return Response({'method': 'None'})

    @staticmethod
    def post(request):
        return Response({'method': 'None'})

    @staticmethod
    def retrieve(request, pk=None):
        models = DistrictProfile.objects.get(id=pk)
        serializer = DistrictUserSerializer(models, context={'request': request})
        return Response({'error': False, 'data': serializer.data})


class DistrictUserViewSet(ViewSet):
    @staticmethod
    def list(request):
        return Response({'method': 'None'})

    @staticmethod
    def retrieve(request, pk=None):
        models = DistrictProfile.objects.filter(district_fk=pk)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = DistrictUserSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def post(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Maglumat ýok!'})
            data = request.data
            try:
                models = Users.objects.get(username=data['username'])
                return Response({'error': True, 'message': 'Bu Username öň bar!'})
            except:
                pass
            try:
                models = Users.objects.get(email=data['email'])
                return Response({'error': True, 'message': 'Bu Email öň bar!'})
            except:
                pass
            model = Users.objects.create_user(username=data['username'], password=data['password'], email=data['email'],
                                         first_name=data['first_name'], last_name=data['last_name'],
                                         third_name=data['third_name'], user_type=4)
            model.districtprofile.district_fk = District.objects.get(id=data['district'])
            model.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Maglumat ýok!'})
            data = request.data
            model = DistrictProfile.objects.get(id=pk)
            model.user.username = data['username']
            model.user.first_name = data['first_name']
            model.user.last_name = data['last_name']
            model.user.email = data['email']
            model.user.third_name = data['third_name']
            model.user.save()
            return Response({'error': False, 'message': 'Maglumat üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        model = get_object_or_404(DistrictProfile.objects.all(), pk=pk)
        model.delete()
        return Response({'error': False})


# Provinvial User
class ProvincialUserDistrictViewSet(ViewSet):
    @staticmethod
    def list(request):
        return Response({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = District.objects.filter(provincial_fk=pk)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = DistrictSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})


Model_list = DistrictViewSet.as_view({'get': 'list'})
Model_create = DistrictViewSet.as_view({'post': 'create'})
Model_update = DistrictViewSet.as_view({'put': 'update'})
Model_delete = DistrictViewSet.as_view({'delete': 'destroy'})
