from django.http import JsonResponse
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from sqlparse.utils import offset

from api.Serializers.Office import OfficeSerializer, StaffSerializer, GetStaffSerializer, StaffUpdateSerializer
from api.models import Office, Staff


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class OfficeViewSet(ViewSet):
    @staticmethod
    def list(request):
        models = Office.objects.filter(office='0')
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = OfficeSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def retrieve(request, pk=None):
        model = Office.objects.get(id=pk)
        serializer = OfficeSerializer(model, context={'request': request})
        return Response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            serializer = OfficeSerializer(data=request.data, context={'request': request})
            serializer.is_valid()
            model = Office()
            model.title = request.data['title']
            model.name = request.data['name']
            model.office = '0'
            model.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            model = get_object_or_404(Office.objects.filter(office='0'), pk=pk)
            serializer = OfficeSerializer(model, data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat Üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        model = get_object_or_404(Office.objects.filter(office='0'), id=pk)
        print(model)
        model.delete()
        return Response({'error': False, 'message': 'Maglumat Pozuldy!'})


class StaffUpdateGetViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': 'None'})

    @staticmethod
    def retrieve(request, pk=None):
        models = Staff.objects.get(id=pk)
        serializer = StaffSerializer(models, context={'request': request})
        return Response({'error': False, 'data': serializer.data})


class StaffViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': 'None'})

    @staticmethod
    def retrieve(request, pk=None):
        models = Staff.objects.filter(office=pk)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = GetStaffSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        print(request.data)
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            serializer = StaffSerializer(data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            model = get_object_or_404(Staff.objects.all(), id=pk)
            serializer = StaffUpdateSerializer(model, data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat Üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        model = get_object_or_404(Staff.objects.all(), id=pk)
        model.delete()
        return Response({'error': False, 'message': 'Maglumat Pozuldy!'})
