from django.http import JsonResponse
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from api.Serializers.Ministry import MinistrySerializer, MinistryUpdateSerializer
from api.models import Users


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class MinistryUserViewSet(ViewSet):
    @staticmethod
    def list(request):
        models = Users.objects.filter(user_type=2)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = MinistryUpdateSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def retrieve(request, pk=None):
        model = get_object_or_404(Users.objects.filter(user_type=2), pk=pk)
        serializer = MinistryUpdateSerializer(model, context={'request': request})
        return Response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            serializer = MinistrySerializer(data=request.data, context={'request': request})
            serializer.is_valid()
            Users.objects.create_user(username=serializer.data['username'], password=serializer.data['password'],
                                      first_name=serializer.data['first_name'], last_name=serializer.data['last_name'],
                                      email=serializer.data['email'], third_name=serializer.data['third_name'],
                                      user_type=2)
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            queryset = Users.objects.filter(user_type=2)
            model = get_object_or_404(queryset, pk=pk)
            serializer = MinistryUpdateSerializer(model, data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat Üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        model = get_object_or_404(Users.objects.filter(user_type=2), pk=pk)
        model.delete()
        return Response({'error': False, 'message': 'Maglumat Pozuldy!'})


Model_list = MinistryUserViewSet.as_view({'get': 'list'})
Model_create = MinistryUserViewSet.as_view({'post': 'create'})
Model_update = MinistryUserViewSet.as_view({'put': 'update'})
Model_delete = MinistryUserViewSet.as_view({'delete': 'destroy'})
