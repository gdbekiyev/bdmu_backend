from ctypes import py_object

from django.http import JsonResponse
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from api.Serializers.HigherEducationInstitute import HigherEducationInstituteSerializer, \
    HigherEducationInstituteGetSerializer
from api.models import HigherEducationInstituteOffice, Office


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class HigherEducationInstituteGetViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = HigherEducationInstituteOffice.objects.get(pk=pk)
        serializer = HigherEducationInstituteSerializer(models, context={'request': request})
        return Response({'error': False, 'data': serializer.data})


class HigherEducationInstituteViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = HigherEducationInstituteOffice.objects.filter(province=pk)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = HigherEducationInstituteGetSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            data = request.data
            print(data)
            model = Office.objects.create(name=data['name'], title=data['title'], office=1,
                                          phone_number=data['phone_number'], address=data['address'],
                                          languages_taught=data['languages_taught'],
                                          student_capacity=data['student_capacity'],
                                          number_of_classrooms=data['number_of_classrooms'],
                                          opened_year=data['opened_year'], email=data['email'],
                                          cafeteria=data['cafeteria'])
            model.highereducationinstituteoffice.province_id = data['district']
            model.highereducationinstituteoffice.sports_playground = data['sports_playground']
            model.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            data = request.data
            print(data)
            model = HigherEducationInstituteOffice.objects.get(id=pk)
            model.office.name = data['name']
            model.office.title = data['title']
            model.office.phone_number = data['phone_number']
            model.office.address = data['address']
            model.office.languages_taught = data['languages_taught']
            model.office.student_capacity = data['student_capacity']
            model.office.number_of_classrooms = data['number_of_classrooms']
            model.office.opened_year = data['opened_year']
            model.office.email = data['email']
            model.office.cafeteria = data['cafeteria']
            model.sports_playground = data['sports_playground']
            model.save()
            model.office.save()
            return Response({'error': False, 'message': 'Maglumat üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        HigherEducationInstituteOffice.objects.get(pk=pk).delete()
        return Response({'error': False, 'message': 'Maglumat pozuldy'})


Model_list = HigherEducationInstituteViewSet.as_view({'get': 'list'})
Model_create = HigherEducationInstituteViewSet.as_view({'post': 'create'})
Model_update = HigherEducationInstituteViewSet.as_view({'put': 'update'})
Model_delete = HigherEducationInstituteViewSet.as_view({'delete': 'destroy'})
