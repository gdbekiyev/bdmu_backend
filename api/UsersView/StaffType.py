from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from api.Serializers.StaffType import StaffTypeSerializer
from api.models import StaffType


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class StaffTypeViewSet(ViewSet):
    @staticmethod
    def list(request):
        models = StaffType.objects.all()
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = StaffTypeSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def retrieve(request, pk=None):
        model = StaffType.objects.get(id=pk)
        serializer = StaffTypeSerializer(model, context={'request': request})
        return Response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            serializer = StaffTypeSerializer(data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            model = get_object_or_404(StaffType.objects.all(), pk=pk)
            serializer = StaffTypeSerializer(model, data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat Üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        model = get_object_or_404(StaffType.objects.all(), id=pk)
        print(model)
        model.delete()
        return Response({'error': False, 'message': 'Maglumat Pozuldy!'})