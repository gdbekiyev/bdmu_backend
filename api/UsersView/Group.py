from django.http import JsonResponse
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from api.Serializers.GroupClassSerializers import GroupClassSerializer, GroupStaffSerializer
from api.models import Staff, GroupClass


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class GroupStaffListViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = Staff.objects.filter(office_id=pk)
        serializer = GroupStaffSerializer(models, many=True, context={'request': request})
        return Response({'error': False, 'data': serializer.data})


class GetGroupGroupViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = GroupClass.objects.get(pk=pk)
        serializer = GroupClassSerializer(models, context={'request': request})
        return Response({'error': False, 'data': serializer.data})


class GroupClassViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = GroupClass.objects.filter(office_fk=pk)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = GroupClassSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            data = request.data
            model = GroupClass()
            model.name = data['name']
            model.office_fk_id = data['office_fk']
            model.grade_year = data['grade_year']
            model.staff_id_id = data['staff_id']
            model.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            data = request.data
            print(data)
            model = GroupClass.objects.get(id=pk)
            model.name = data['name']
            model.grade_year = data['grade_year']
            model.staff_id_id = data['staff_id']
            model.save()
            return Response({'error': False, 'message': 'Maglumat üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        GroupClass.objects.get(pk=pk).delete()
        return Response({'error': False, 'message': 'Maglumat pozuldy'})


Model_list = GroupClassViewSet.as_view({'get': 'list'})
Model_create = GroupClassViewSet.as_view({'post': 'create'})
Model_update = GroupClassViewSet.as_view({'put': 'update'})
Model_delete = GroupClassViewSet.as_view({'delete': 'destroy'})
