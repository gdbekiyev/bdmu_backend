from django.http import JsonResponse
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from api.Serializers.KindergartenLearner import GetKindergartenLearnerUserSerializer, KindergartenLearnerUserSerializer
from api.models import KindergartenProfile, Learner


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class KindergartenLearnerGetViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = KindergartenProfile.objects.get(pk=pk)
        serializer = KindergartenLearnerUserSerializer(models, context={'request': request})
        return Response({'error': False, 'data': serializer.data})


class KindergartenLearnerViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = KindergartenProfile.objects.filter(learner__group__office_fk=pk)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = GetKindergartenLearnerUserSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            data = request.data
            model = Learner.objects.create(
                system_id=data['system_id'],
                group_id=data['group'],
                name=data['name'],
                surname=data['surname'],
                patronymic=data['patronymic'],
                gender=data['gender'],
                matriculation_date=data['matriculation_date'],
                matriculation_order_number=data['matriculation_order_number'],
                permanent_address=data['permanent_address'],
                phone_number=data['phone_number'],
                date_of_birth=data['date_of_birth'],
                place_of_birth=data['place_of_birth'],
                nationality=data['nationality'],
                citizenship=data['citizenship'],
                health_status=data['health_status'],
                guardian_name=data['guardian_name'],
                guardian_organization=data['guardian_organization'],
                guardian_phone=data['guardian_phone'],
                guardian_email=data['guardian_email'],
                is_graduate=data['is_graduate'],
                graduated_date=data['graduated_date'],
                learner=1
            )
            model.kindergartenprofile.birth_certificate_id = data['birth_certificate_id']
            model.kindergartenprofile.student_number = data['student_number']
            model.kindergartenprofile.study_language = data['study_language']
            model.kindergartenprofile.group = data['grouplist']
            model.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        # try:
        if request.data is None or request.data == "":
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
        data = request.data
        model = Learner.objects.get(id=pk)
        model.system_id = data['system_id']
        model.group_id = data['group']
        model.name = data['name']
        model.surname = data['surname']
        model.patronymic = data['patronymic']
        model.gender = data['gender']
        model.matriculation_date = data['matriculation_date']
        model.matriculation_order_number = data['matriculation_order_number']
        model.permanent_address = data['permanent_address']
        model.phone_number = data['phone_number']
        model.date_of_birth = data['date_of_birth']
        model.place_of_birth = data['place_of_birth']
        model.nationality = data['nationality']
        model.citizenship = data['citizenship']
        model.health_status = data['health_status']
        model.guardian_name = data['guardian_name']
        model.guardian_organization = data['guardian_organization']
        model.guardian_phone = data['guardian_phone']
        model.guardian_email = data['guardian_email']
        model.is_graduate = data['is_graduate']
        model.graduated_date = data['graduated_date']
        model.kindergartenprofile.birth_certificate_id = data['birth_certificate_id']
        model.kindergartenprofile.student_number = data['student_number']
        model.kindergartenprofile.study_language = data['study_language']
        model.kindergartenprofile.group = data['grouplist']
        model.save()
        model.kindergartenprofile.save()
        return Response({'error': False, 'message': 'Maglumat üýtgedildi!'})
        # except:
        #     return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        KindergartenProfile.objects.get(pk=pk).delete()
        return Response({'error': False, 'message': 'Maglumat pozuldy'})


Model_list = KindergartenLearnerViewSet.as_view({'get': 'list'})
Model_create = KindergartenLearnerViewSet.as_view({'post': 'create'})
Model_update = KindergartenLearnerViewSet.as_view({'put': 'update'})
Model_delete = KindergartenLearnerViewSet.as_view({'delete': 'destroy'})
