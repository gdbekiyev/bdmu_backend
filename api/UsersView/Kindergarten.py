from django.http import JsonResponse
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from api.Serializers.Kindergarten import KindergartenSerializer, KindergartenGetSerializer
from api.models import KindergartenOffice, Office


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class KindergartenGetViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = KindergartenOffice.objects.get(pk=pk)
        serializer = KindergartenSerializer(models, context={'request': request})
        return Response({'error': False, 'data': serializer.data})


class KindergartenViewSet(ViewSet):
    @staticmethod
    def list(request):
        return JsonResponse({'method': None})

    @staticmethod
    def retrieve(request, pk=None):
        models = KindergartenOffice.objects.filter(district=pk)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = KindergartenGetSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            data = request.data
            print(data)
            model = Office.objects.create(name=data['name'], title=data['title'], office=5,
                                          phone_number=data['phone_number'], address=data['address'],
                                          languages_taught=data['languages_taught'],
                                          student_capacity=data['student_capacity'],
                                          number_of_classrooms=data['number_of_classrooms'],
                                          opened_year=data['opened_year'], email=data['email'],
                                          cafeteria=data['cafeteria'])
            model.kindergartenoffice.district_id = data['district']
            model.kindergartenoffice.number_of_beds = data['number_of_beds']
            model.kindergartenoffice.swimming_pool = data['swimming_pool']
            model.kindergartenoffice.playground = data['playground']
            model.kindergartenoffice.toys = data['toys']
            model.kindergartenoffice.entertainment = data['entertainment']
            model.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            data = request.data
            model = KindergartenOffice.objects.get(id=pk)
            model.office.name = data['name']
            model.office.title = data['title']
            model.office.phone_number = data['phone_number']
            model.office.address = data['address']
            model.office.languages_taught = data['languages_taught']
            model.office.student_capacity = data['student_capacity']
            model.office.number_of_classrooms = data['number_of_classrooms']
            model.office.opened_year = data['opened_year']
            model.office.email = data['email']
            model.office.cafeteria = data['cafeteria']
            model.office.district_id = data['district']
            model.number_of_beds = data['number_of_beds']
            model.swimming_pool = data['swimming_pool']
            model.playground = data['playground']
            model.toys = data['toys']
            model.entertainment = data['entertainment']
            model.save()
            model.office.save()
            return Response({'error': False, 'message': 'Maglumat üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        KindergartenOffice.objects.get(pk=pk).delete()
        return Response({'error': False, 'message': 'Maglumat pozuldy'})


Model_list = KindergartenViewSet.as_view({'get': 'list'})
Model_create = KindergartenViewSet.as_view({'post': 'create'})
Model_update = KindergartenViewSet.as_view({'put': 'update'})
Model_delete = KindergartenViewSet.as_view({'delete': 'destroy'})
