from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from api.models import Provincial, District, DistrictProfile


class AdminHomeViewSet(ViewSet):
    @staticmethod
    def list(request):
        data = []
        return Response({'data': data})


Model_list = AdminHomeViewSet.as_view({'get': 'list'})
