from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from api.Serializers.DirectorSerializers import DirectorSerializer
from api.models import DirectorProfile, Users, KindergartenOffice, SchoolOffice


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class GetDirectorUserViewSet(ViewSet):
    @staticmethod
    def list(request):
        return Response({'method': 'None'})

    @staticmethod
    def post(request):
        return Response({'method': 'None'})

    @staticmethod
    def retrieve(request, pk=None):
        models = DirectorProfile.objects.get(id=pk)
        serializer = DirectorSerializer(models, context={'request': request})
        return Response({'error': False, 'data': serializer.data})


class DirectorUserViewSet(ViewSet):
    @staticmethod
    def list(request):
        return Response({'method': 'None'})

    @staticmethod
    def post(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Maglumat ýok!'})
            data = request.data
            print(request.data)
            try:
                models = Users.objects.get(username=data['username'])
                return Response({'error': True, 'message': 'Bu Username öň bar!'})
            except:
                pass
            try:
                models = Users.objects.get(email=data['email'])
                return Response({'error': True, 'message': 'Bu Email öň bar!'})
            except:
                pass
            model = Users.objects.create_user(username=data['username'], password=data['password'], email=data['email'],
                                              first_name=data['first_name'], last_name=data['last_name'],
                                              third_name=data['third_name'], user_type=5)

            if data['model'] == 5:
                model.directorprofile.office_fk_id = KindergartenOffice.objects.get(id=data['office']).office_id
            elif data['model'] == 4:
                model.directorprofile.office_fk_id = SchoolOffice.objects.get(id=data['office']).office_id
            model.directorprofile.director = data['type']
            model.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Maglumat ýok!'})
            data = request.data
            model = DirectorProfile.objects.get(id=pk)
            model.user.username = data['username']
            model.user.first_name = data['first_name']
            model.user.last_name = data['last_name']
            model.user.email = data['email']
            model.user.third_name = data['third_name']
            model.user.save()
            return Response({'error': False, 'message': 'Maglumat üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        model = get_object_or_404(DirectorProfile.objects.all(), pk=pk)
        model.delete()
        return Response({'error': False})

    @staticmethod
    def retrieve(request, pk=None):
        models = DirectorProfile.objects.filter(office_fk_id=pk)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = DirectorSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})
