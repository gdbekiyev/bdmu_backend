from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from api.Serializers.Provincial import ProvincialSerializer, ProvincialUserSerializer
from api.models import Users, Provincial, ProvincialProfile


class ResponsePagination(PageNumberPagination):
    page_query_param = 'page'
    page_size = 7
    max_page_size = 7


class ProvincialViewSet(ViewSet):
    @staticmethod
    def list(request):
        models = Provincial.objects.all()
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = ProvincialSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def retrieve(request, pk=None):
        model = Provincial.objects.get(id=pk)
        serializer = ProvincialSerializer(model, context={'request': request})
        return Response({'error': False, 'data': serializer.data})

    @staticmethod
    def create(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            serializer = ProvincialSerializer(data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})
            queryset = Provincial.objects.all()
            model = get_object_or_404(queryset, pk=pk)
            serializer = ProvincialSerializer(model, data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response({'error': False, 'message': 'Maglumat Üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        model = get_object_or_404(Provincial.objects.all(), pk=pk)
        model.delete()
        return Response({'error': False, 'message': 'Maglumat Pozuldy!'})


class GetProvincialUserViewSet(ViewSet):
    @staticmethod
    def list(request):
        return Response({'method': 'None'})

    @staticmethod
    def post(request):
        return Response({'method': 'None'})

    @staticmethod
    def retrieve(request, pk=None):
        models = ProvincialProfile.objects.get(id=pk)
        serializer = ProvincialUserSerializer(models, context={'request': request})
        return Response({'error': False, 'data': serializer.data})


class ProvincialUserViewSet(ViewSet):
    @staticmethod
    def list(request):
        return Response({'method': 'None'})

    @staticmethod
    def retrieve(request, pk=None):
        models = ProvincialProfile.objects.filter(provincial_fk=pk)
        paginator = ResponsePagination()
        results = paginator.paginate_queryset(models, request)
        serializer = ProvincialUserSerializer(results, many=True, context={'request': request})
        return paginator.get_paginated_response({'error': False, 'data': serializer.data})

    @staticmethod
    def post(request):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Maglumat ýok!'})
            data = request.data
            try:
                models = Users.objects.get(username=data['username'])
                return Response({'error': True, 'message': 'Bu Username öň bar!'})
            except:
                pass
            try:
                models = Users.objects.get(email=data['email'])
                return Response({'error': True, 'message': 'Bu Email öň bar!'})
            except:
                pass
            model = Users.objects.create_user(username=data['username'], password=data['password'], email=data['email'],
                                         first_name=data['first_name'], last_name=data['last_name'],
                                         third_name=data['third_name'], user_type=3)
            model.provincialprofile.provincial_fk = Provincial.objects.get(id=data['provincial'])
            model.save()
            return Response({'error': False, 'message': 'Maglumat goşuldy!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def update(request, pk=None):
        try:
            if request.data is None or request.data == "":
                return Response({'error': True, 'message': 'Maglumat ýok!'})
            data = request.data
            model = ProvincialProfile.objects.get(id=pk)
            model.user.username = data['username']
            model.user.first_name = data['first_name']
            model.user.last_name = data['last_name']
            model.user.email = data['email']
            model.user.third_name = data['third_name']
            model.user.save()
            return Response({'error': False, 'message': 'Maglumat üýtgedildi!'})
        except:
            return Response({'error': True, 'message': 'Ýalňyşlyk ýüze çykdy!'})

    @staticmethod
    def destroy(request, pk=None):
        model = get_object_or_404(ProvincialProfile.objects.all(), pk=pk)
        model.delete()
        return Response({'error': False})


Model_list = ProvincialViewSet.as_view({'get': 'list'})
Model_create = ProvincialViewSet.as_view({'post': 'create'})
Model_update = ProvincialViewSet.as_view({'put': 'update'})
Model_delete = ProvincialViewSet.as_view({'delete': 'destroy'})
