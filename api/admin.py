from django.contrib import admin

from api.models import Users, Provincial, ProvincialProfile, DistrictProfile, Staff, KindergartenOffice, Office, \
    SchoolOffice, PrimaryVocationSchoolOffice, SecondaryProfessionalSchoolOffice, DirectorProfile, GroupClass, \
    KindergartenProfile, SchoolProfile

admin.site.register(Users)
admin.site.register(Provincial)
admin.site.register(ProvincialProfile)
admin.site.register(DistrictProfile)
admin.site.register(Staff)
admin.site.register(KindergartenOffice)
admin.site.register(SchoolOffice)
admin.site.register(PrimaryVocationSchoolOffice)
admin.site.register(SecondaryProfessionalSchoolOffice)
admin.site.register(Office)
admin.site.register(DirectorProfile)
admin.site.register(GroupClass)
admin.site.register(KindergartenProfile)
admin.site.register(SchoolProfile)
