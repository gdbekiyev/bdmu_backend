from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Provincial(models.Model):
    name = models.CharField(max_length=250, null=True)
    title = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.title

    def delete(self, *args, **kwargs):
        for i in ProvincialProfile.objects.filter(provincial_fk=self):
            Users.objects.get(id=i.user.id).delete()
            i.delete()
        return super(Provincial, self).delete(kwargs)


class District(models.Model):
    name = models.CharField(max_length=250, null=True)
    title = models.CharField(max_length=100, null=True)
    provincial_fk = models.ForeignKey(Provincial, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def delete(self, *args, **kwargs):
        for i in DistrictProfile.objects.filter(district_fk=self):
            Users.objects.get(id=i.user.id).delete()
            i.delete()
        return super(District, self).delete(kwargs)


class Users(AbstractUser):
    user_type_list = (
        ('1', 'Administrator'),
        ('2', 'Türkmenistanyň Bilim Ministrligi'),
        ('3', 'Welaýat bilim müdirligi'),
        ('4', 'Etrap bilim müdirligi'),
        ('5', 'Direktor'),
    )
    third_name = models.CharField(max_length=100, blank=True)
    user_type = models.CharField(max_length=10, choices=user_type_list, default=1)

    def __str__(self):
        return self.email


class ProvincialProfile(models.Model):
    user = models.OneToOneField(Users, on_delete=models.CASCADE)
    provincial_fk = models.ForeignKey(Provincial, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user.first_name + " " + self.user.last_name)

    def delete(self, *args, **kwargs):
        Users.objects.get(id=self.user.id).delete()
        return super(ProvincialProfile, self).delete(kwargs)


class DistrictProfile(models.Model):
    user = models.OneToOneField(Users, on_delete=models.CASCADE)
    district_fk = models.ForeignKey(District, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user.first_name + " " + self.user.last_name)

    def delete(self, *args, **kwargs):
        Users.objects.get(id=self.user.id).delete()
        return super(DistrictProfile, self).delete(kwargs)


class Office(models.Model):
    office_list = (
        ('0', 'Ministrlik'),
        ('1', 'Ýokary okuw jaýy'),
        ('2', 'Orta hünär mekdep'),
        ('3', 'Hünär mekdep'),
        ('4', 'Orta mekdep'),
        ('5', 'Çagalar bagy'),
    )
    name = models.CharField(max_length=500, null=True)
    title = models.CharField(max_length=100, null=True)
    office = models.CharField(max_length=10, choices=office_list, null=True)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    address = models.CharField(max_length=1000, null=True, blank=True)
    languages_taught = models.CharField(max_length=100, null=True, blank=True)
    student_capacity = models.IntegerField(default=0)
    number_of_classrooms = models.IntegerField(default=0)
    opened_year = models.IntegerField(default=0)
    email = models.CharField(max_length=254, null=True, blank=True)
    cafeteria = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return self.name

    def delete(self, *args, **kwargs):
        for i in DirectorProfile.objects.filter(office_fk=self):
            Users.objects.filter(id=i.user.id).delete()
            i.delete()
        return super(Office, self).delete(args, kwargs)


class HigherEducationInstituteOffice(models.Model):
    office = models.OneToOneField(Office, on_delete=models.CASCADE)
    province = models.ForeignKey(Provincial, on_delete=models.CASCADE)
    sports_playground = models.BooleanField()
    foreign_languages_taught = models.TextField()

    def __str__(self):
        return self.office.title


class SecondaryProfessionalSchoolOffice(models.Model):
    office = models.OneToOneField(Office, on_delete=models.CASCADE)
    province = models.ForeignKey(Provincial, on_delete=models.CASCADE)
    sports_playground = models.BooleanField()

    def __str__(self):
        return self.office.title


class PrimaryVocationSchoolOffice(models.Model):
    office = models.OneToOneField(Office, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    sports_playground = models.BooleanField()

    def __str__(self):
        return self.office.title


class SchoolOffice(models.Model):
    office = models.OneToOneField(Office, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    sports_playground = models.BooleanField()

    def __str__(self):
        return self.office.title


class KindergartenOffice(models.Model):
    office = models.OneToOneField(Office, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    number_of_beds = models.IntegerField()
    swimming_pool = models.BooleanField(default=False)
    playground = models.BooleanField(default=False)
    toys = models.BooleanField(default=False)
    entertainment = models.BooleanField(default=False)


class DirectorProfile(models.Model):
    director_list = (
        ('1', 'Direktor'),
        ('2', 'Orunbasary'),
        ('3', 'Kömekçi')
    )
    user = models.OneToOneField(Users, on_delete=models.CASCADE)
    office_fk = models.ForeignKey(Office, on_delete=models.CASCADE)
    director = models.CharField(choices=director_list, max_length=10, null=True)

    def __str__(self):
        return str(self.user.first_name + " " + self.user.last_name)

    def delete(self, *args, **kwargs):
        for i in Users.objects.filter(id=self.user_id):
            i.delete()
        for j in Office.objects.filter(id=self.office_fk_id):
            j.delete()
            return super(DirectorProfile, self).delete(args, kwargs)


class StaffType(models.Model):
    name = models.CharField(max_length=150, null=True)

    def __str__(self):
        return self.name


Nationality_List = (
    ('1', 'Türkmen'),
    ('2', 'Rus'),
    ('3', 'Özbek'),
    ('4', 'Gazak'),
    ('5', 'Azerbeýjan'),
    ('6', 'Täjik'),
)


class Staff(models.Model):
    gender_list = (
        ('1', 'Erkek'),
        ('2', 'Aýal'),
    )
    office = models.ForeignKey(Office, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    third_name = models.CharField(max_length=100, blank=True)
    gender = models.CharField(max_length=10, null=True, choices=gender_list)
    joined_date = models.DateField(auto_created=True)
    order_number = models.CharField(max_length=150, null=True, blank=True)
    permanent_address = models.CharField(max_length=150, null=True, blank=True)
    phone_number = models.CharField(max_length=100, null=True, blank=True)
    date_of_birth = models.DateField()
    nationality = models.CharField(max_length=10, choices=Nationality_List, null=True)
    citizenship = models.CharField(max_length=200, null=True, blank=True)
    health_status = models.CharField(max_length=10, choices=(('1', 'Healthy'), ('2', 'Invalid')), null=True)
    occupation = models.CharField(max_length=100, null=True, blank=True)
    education_level = models.CharField(max_length=10, choices=(('1', 'None'), ('2', 'middle'), ('3', 'High')),
                                       null=True)
    highest_degree_earned = models.CharField(max_length=10,
                                             choices=(('1', 'None'), ('2', 'bachelor'), ('3', 'master'), ('4', 'phd')),
                                             null=True)
    place_of_last_education = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return str(self.first_name + " " + self.last_name)


class GroupClass(models.Model):
    name = models.CharField(max_length=150, null=True)
    office_fk = models.ForeignKey(Office, on_delete=models.CASCADE)
    grade_year = models.IntegerField(null=True, blank=True)
    staff_id = models.ForeignKey(Staff, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return str(self.name + " / " + self.office_fk.name)


class Learner(models.Model):
    system_id = models.CharField(max_length=250, null=True, blank=True)
    group = models.ForeignKey(GroupClass, on_delete=models.CASCADE)
    name = models.CharField(max_length=250, null=True, blank=True)
    surname = models.CharField(max_length=250, null=True, blank=True)
    patronymic = models.CharField(max_length=250, null=True, blank=True)
    gender_list = (
        ('1', 'Male'),
        ('1', 'Female'),
    )
    gender = models.CharField(max_length=10, choices=gender_list, null=True)
    matriculation_date = models.DateField()
    matriculation_order_number = models.CharField(max_length=250, null=True, blank=True)
    permanent_address = models.CharField(max_length=500, null=True, blank=True)
    phone_number = models.CharField(max_length=15, blank=True, null=True)
    date_of_birth = models.DateField()
    place_of_birth = models.CharField(max_length=500, null=True, blank=True)
    nationality = models.CharField(max_length=50, null=True, blank=True)
    citizenship = models.CharField(max_length=200, null=True, blank=True)
    health_status_list = (
        ('1', 'Healthy'),
        ('2', 'Invalid'),
    )
    health_status = models.CharField(max_length=10, choices=health_status_list, null=True)
    guardian_name = models.CharField(max_length=100, null=True, blank=True)
    guardian_organization = models.CharField(max_length=250, null=True, blank=True)
    guardian_phone = models.CharField(max_length=15, null=True, blank=True)
    guardian_email = models.CharField(max_length=254, null=True, blank=True)
    is_graduate = models.BooleanField()
    graduated_date = models.DateField()
    learner = models.IntegerField(null=True)

    def __str__(self):
        return self.system_id


Study_Language = (
    ('1', 'Turkmen'),
    ('2', 'English'),
    ('3', 'Russian'),
)

Group_List = (
    ('1', 'A'),
    ('2', 'B'),
    ('3', 'Ç'),
    ('4', 'D'),
    ('5', 'E'),
    ('6', 'Ä'),
    ('7', 'F'),
)


class KindergartenProfile(models.Model):
    learner = models.OneToOneField(Learner, on_delete=models.CASCADE)
    birth_certificate_id = models.IntegerField()
    student_number = models.CharField(max_length=100, null=True, blank=True)
    study_language = models.CharField(max_length=10, choices=Study_Language, null=True)
    group = models.CharField(max_length=10, choices=Group_List, null=True)


class SchoolProfile(models.Model):
    learner = models.OneToOneField(Learner, on_delete=models.CASCADE)
    birth_certificate_id = models.IntegerField()
    student_number = models.CharField(max_length=100, null=True, blank=True)
    study_language = models.CharField(max_length=10, choices=Study_Language, null=True)
    group = models.CharField(max_length=10, choices=Group_List, null=True)


class PrimaryVocationalSchoolProfile(models.Model):
    learner = models.OneToOneField(Learner, on_delete=models.CASCADE)
    birth_certificate_id = models.IntegerField()
    student_number = models.CharField(max_length=100, null=True, blank=True)
    study_language = models.CharField(max_length=10, choices=Study_Language, null=True)
    passport_id = models.CharField(max_length=100, null=True, blank=True)
    last_surname = models.CharField(max_length=100, null=True, blank=True)
    work_list = (
        ('1', 'Yes'),
        ('2', 'No'),
    )
    working = models.CharField(max_length=100, choices=work_list, null=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    shift_list = (
        ('1', 'morning'),
        ('2', 'afternoon'),
    )
    shift = models.CharField(max_length=100, choices=shift_list, null=True)
    study_type_list = (
        ('1', 'gaybana'),
        ('2', 'gundizki'),
    )
    study_type = models.CharField(max_length=100, choices=study_type_list, null=True)
    achievenment_garde = models.CharField(max_length=100, null=True, blank=True)
    major = models.CharField(max_length=100, null=True, blank=True)
    faculty = models.CharField(max_length=100, null=True, blank=True)
    group_list = (
        ('1', 'I'),
        ('2', 'II'),
        ('3', 'III'),
        ('4', 'IV'),
        ('5', 'V'),

    )
    group = models.CharField(max_length=100, choices=group_list, null=True)


class SecondaryProfessionalSchoolProfile(models.Model):
    learner = models.OneToOneField(Learner, on_delete=models.CASCADE)
    birth_certificate_id = models.IntegerField()
    student_number = models.CharField(max_length=100, null=True, blank=True)
    study_language = models.CharField(max_length=10, choices=Study_Language, null=True)
    passport_id = models.CharField(max_length=100, null=True, blank=True)
    last_surname = models.CharField(max_length=100, null=True, blank=True)
    work_list = (
        ('1', 'Yes'),
        ('2', 'No'),
    )
    working = models.CharField(max_length=100, choices=work_list, null=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    shift_list = (
        ('1', 'morning'),
        ('2', 'afternoon'),
    )
    shift = models.CharField(max_length=100, choices=shift_list)
    study_type_list = (
        ('1', 'gaybana'),
        ('2', 'gundizki'),
    )
    study_type = models.CharField(max_length=100, choices=study_type_list)
    achievenment_garde = models.CharField(max_length=100, null=True, blank=True)
    major = models.CharField(max_length=100, null=True, blank=True)
    faculty = models.CharField(max_length=100, null=True, blank=True)
    group_list = (
        ('1', 'I'),
        ('2', 'II'),
        ('3', 'III'),
        ('4', 'IV'),
        ('5', 'V'),

    )
    group = models.CharField(max_length=100, choices=group_list)


class HigherEduactionInstitutionsProfile(models.Model):
    learner = models.OneToOneField(Learner, on_delete=models.CASCADE)
    birth_certificate_id = models.IntegerField()
    student_number = models.CharField(max_length=100, null=True, blank=True)
    study_language = models.CharField(max_length=10, choices=Study_Language)
    passport_id = models.CharField(max_length=100, null=True, blank=True)
    last_surname = models.CharField(max_length=100, null=True, blank=True)
    work_list = (
        ('1', 'Yes'),
        ('2', 'No'),
    )
    working = models.CharField(max_length=100, choices=work_list)
    email = models.CharField(max_length=100, null=True, blank=True)
    shift_list = (
        ('1', 'morning'),
        ('2', 'afternoon'),
    )
    shift = models.CharField(max_length=100, choices=shift_list)
    study_type_list = (
        ('1', 'gaybana'),
        ('2', 'gundizki'),
    )
    study_type = models.CharField(max_length=100, choices=study_type_list)
    achievenment_garde = models.CharField(max_length=100, null=True, blank=True)
    major = models.CharField(max_length=100, null=True, blank=True)
    faculty = models.CharField(max_length=100, null=True, blank=True)
    group_list = (
        ('1', 'I'),
        ('2', 'II'),
        ('3', 'III'),
        ('4', 'IV'),
        ('5', 'V'),

    )
    group = models.CharField(max_length=100, choices=group_list)


class ResetPasswordMail(models.Model):
    email = models.OneToOneField(Users, on_delete=models.CASCADE)
    password = models.IntegerField(blank=True)

    def __str__(self):
        return self.email


@receiver(post_save, sender=Users)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        if instance.user_type == 1:
            ResetPasswordMail.objects.create(email=instance, password=0)
        if instance.user_type == 2:
            ResetPasswordMail.objects.create(email=instance, password=0)
        if instance.user_type == 3:
            ResetPasswordMail.objects.create(email=instance, password=0)
            ProvincialProfile.objects.create(user=instance, provincial_fk=Provincial.objects.all().first())
        if instance.user_type == 4:
            ResetPasswordMail.objects.create(email=instance, password=0)
            DistrictProfile.objects.create(user=instance, district_fk=District.objects.first())
        if instance.user_type == 5:
            ResetPasswordMail.objects.create(email=instance, password=0)
            DirectorProfile.objects.create(user=instance, office_fk=Office.objects.first(),
                                           director=1)


@receiver(post_save, sender=Office)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        if instance.office == 1:
            HigherEducationInstituteOffice.objects.create(
                office=instance, province=Provincial.objects.all().first(), sports_playground=False, )
        if instance.office == 2:
            SecondaryProfessionalSchoolOffice.objects.create(
                office=instance, province=Provincial.objects.all().first(), sports_playground=False, )
        if instance.office == 3:
            PrimaryVocationSchoolOffice.objects.create(
                office=instance, district=District.objects.all().first(), sports_playground=False)
        if instance.office == 4:
            SchoolOffice.objects.create(
                office=instance, district=District.objects.all().first(), sports_playground=False)
        if instance.office == 5:
            KindergartenOffice.objects.create(
                office=instance, district=District.objects.all().first(), number_of_beds=0, swimming_pool=False,
                playground=False, toys=False, entertainment=False)


@receiver(post_save, sender=Learner)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        if instance.learner == 1:
            KindergartenProfile.objects.create(learner=instance, birth_certificate_id=0, student_number="0",
                                               study_language="0", group="1")
        if instance.learner == 2:
            SchoolProfile.objects.create(learner=instance, birth_certificate_id=0, student_number="0",
                                         study_language="0", group="1")


@receiver(post_save, sender=Users)
def save_user_profile(sender, instance, **kwargs):
    if instance.user_type == 1:
        instance.resetpasswordmail.save()
    if instance.user_type == 2:
        instance.resetpasswordmail.save()
    if instance.user_type == 3:
        instance.resetpasswordmail.save()
        instance.provincialprofile.save()
    if instance.user_type == 4:
        instance.resetpasswordmail.save()
        instance.districtprofile.save()
    if instance.user_type == 5:
        instance.resetpasswordmail.save()
        instance.directorprofile.save()


@receiver(post_save, sender=Office)
def save_user_profile(sender, instance, **kwargs):
    if instance.office == 1:
        instance.highereducationinstituteoffice.save()
    if instance.office == 2:
        instance.secondaryprofessionalschooloffice.save()
    if instance.office == 3:
        instance.primaryvocationschooloffice.save()
    if instance.office == 4:
        instance.schooloffice.save()
    if instance.office == 5:
        instance.kindergartenoffice.save()


@receiver(post_save, sender=Learner)
def save_user_profile(sender, instance, **kwargs):
    if instance.learner == 1:
        instance.kindergartenprofile.save()
