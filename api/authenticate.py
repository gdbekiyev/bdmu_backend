import jwt
from django.conf import settings
from django.contrib import auth
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from api.models import ResetPasswordMail, Users


def authenticate(request, email, password):
    UserModel = get_user_model()
    try:
        user = UserModel.objects.get(email=email)
    except UserModel.DoesNotExist:
        return None
    else:
        if user.check_password(password):
            return user
    return None


class LoginView(GenericAPIView):
    permission_classes = ()
    authentication_classes = ()

    @staticmethod
    def post(request):
        data = request.data
        email = data.get('email', '')
        password = data.get('password', '')
        user = authenticate(request, email, password)
        if user:
            auth_token = None
            if user.user_type == '1':
                auth_token = jwt.encode(
                    {'email': user.email, 'user_type': user.user_type}, settings.SECRET_KEY,
                    algorithm="HS256")
            elif user.user_type == '2':
                auth_token = jwt.encode(
                    {'email': user.email, 'user_type': user.user_type}, settings.SECRET_KEY,
                    algorithm="HS256")
            elif user.user_type == '3':
                auth_token = jwt.encode(
                    {'email': user.email, 'user_type': user.user_type,
                     'provincial': user.provincialprofile.provincial_fk.id,
                     'name': user.provincialprofile.provincial_fk.title}, settings.SECRET_KEY,
                    algorithm="HS256")
            elif user.user_type == '4':
                auth_token = jwt.encode(
                    {'email': user.email, 'user_type': user.user_type,
                     'district': user.districtprofile.district_fk.id,
                     'name': user.districtprofile.district_fk.title}, settings.SECRET_KEY,
                    algorithm="HS256")
            elif user.user_type == '5':
                auth_token = jwt.encode(
                    {'email': user.email, 'user_type': user.user_type,
                     'office': user.directorprofile.office_fk_id, 'director': user.directorprofile.office_fk.office,
                     'name': user.directorprofile.office_fk.title}, settings.SECRET_KEY,
                    algorithm="HS256")
            data = {'token': auth_token, "first_name": user.first_name, "last_name": user.last_name}

            return Response(data, status=status.HTTP_200_OK)

        return Response({'Netije': 'Email ýa-da Password ýalňyş'}, status=status.HTTP_401_UNAUTHORIZED)

    def get_queryset(self):
        return Response({'Hello'})


def sendmail(model=None):
    user = ResetPasswordMail.objects.get(id=model)
    subject = 'EMIS account Reset Passowrd'
    message = user.email.first_name + ' ' + user.email.last_name + ' account reset enter password: ' + str(
        user.password)
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [user.email.email]
    send_mail(subject, message, email_from, recipient_list)


class ResetPasswordEmail(GenericAPIView):
    permission_classes = ()
    authentication_classes = ()

    @staticmethod
    def post(request):
        data = request.data
        print(request.data)
        try:
            user = ResetPasswordMail.objects.get(email__email=data['email'])
        except:
            return Response({'error': True})
        import math, random
        kod = ''
        number = '0123456789'
        print("123")
        for i in range(6):
            kod += number[math.floor(random.random() * 10)]
        user.password = int(kod)
        print("123")
        user.save()
        sendmail(user.id)
        print("Success")
        return Response({'error': False})


class ResetUserPassword(GenericAPIView):
    permission_classes = ()
    authentication_classes = ()

    @staticmethod
    def post(request):
        data = request.data
        email = data['email']
        password = int(data['password'])
        try:
            model = ResetPasswordMail.objects.get(email__email=email, password=password)
            return Response({'error': False, 'ID': model.email.id})
        except:
            return Response({'error': True})


class NewPassword(GenericAPIView):
    permission_classes = ()
    authentication_classes = ()

    @staticmethod
    def post(request):
        data = request.data
        UserID = int(data['UserID'])
        password = data['password']
        model = Users.objects.get(id=UserID)
        model.set_password(raw_password=password)
        model.save()
        return Response({'error': False})
