from django.http import JsonResponse

from api import models
from api.models import Provincial, SchoolProfile, SchoolOffice, KindergartenProfile, KindergartenOffice


def AdminHome(request):
    data = []
    for i in Provincial.objects.all():
        data_list = {'label': i.name}
        school = 0
        kinder = 0
        for j in SchoolOffice.objects.filter(district__provincial_fk=i):
            school += SchoolProfile.objects.filter(learner__group__office_fk=j.office).count()
        data_list['y'] = school
        data.append(data_list)
        # for j in KindergartenOffice.objects.filter(district__provincial_fk=i):
        #     kinder += KindergartenProfile.objects.filter(learner__group__office_fk=j.office).count()
        #     data_list['kinder'] = kinder
    return JsonResponse({'data': data})


def Nationality(request):
    model = []
    for i in models.Nationality_List:
        model_list = {'id': int(i[0]), 'name': i[1]}
        model.append(model_list)
    return JsonResponse({
        'data': model
    })


def Error_Page_404(request, exception):
    return JsonResponse({'status': 404})


def Error_Page_500(request):
    return JsonResponse({'status': 500})
