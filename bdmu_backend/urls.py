from django.contrib import admin
from django.urls import path, include
from django.conf.urls import handler404, handler500
from rest_framework import routers

from api.UsersView.Director import GetDirectorUserViewSet, DirectorUserViewSet
from api.UsersView.District import DistrictViewSet, DistrictUserViewSet, GetDistrictUserViewSet, \
    ProvincialUserDistrictViewSet
from api.UsersView.Group import GroupClassViewSet, GroupStaffListViewSet, GetGroupGroupViewSet
from api.UsersView.HigherEducationInstitute import HigherEducationInstituteViewSet, HigherEducationInstituteGetViewSet
from api.UsersView.Home import AdminHomeViewSet
from api.UsersView.Kindergarten import KindergartenViewSet, KindergartenGetViewSet
from api.UsersView.KindergartenLearner import KindergartenLearnerViewSet, KindergartenLearnerGetViewSet
from api.UsersView.Ministry import MinistryUserViewSet
from api.UsersView.MinistryStaff import OfficeViewSet, StaffViewSet, StaffUpdateGetViewSet
from api.UsersView.PrimaryVocationSchool import PrimaryVocationSchoolViewSet, PrimaryVocationSchoolGetViewSet
from api.UsersView.Provincial import ProvincialViewSet, ProvincialUserViewSet, GetProvincialUserViewSet
from api.UsersView.School import SchoolViewSet, SchoolGetViewSet
from api.UsersView.SchoolLearner import SchoolLearnerViewSet, SchoolLearnerGetViewSet
from api.UsersView.SecondaryProfessionalSchool import SecondaryProfessionalSchoolViewSet, \
    SecondaryProfessionalSchoolGetViewSet
from api.UsersView.StaffType import StaffTypeViewSet
from api.authenticate import LoginView, ResetPasswordEmail, ResetUserPassword, NewPassword
from api.views import Nationality, AdminHome

router = routers.DefaultRouter()
router.register('adminhome', AdminHomeViewSet, basename='AdminHome')
router.register('ministry', MinistryUserViewSet, basename='Ministry')
router.register('provincial', ProvincialViewSet, basename='Welayat')
router.register('userprovincial', ProvincialUserViewSet, basename='WelayatUser')
router.register('userprovincialupdate', GetProvincialUserViewSet, basename='WelayatUser')
router.register('district', DistrictViewSet, basename='Etrap')
router.register('provincialuserdistrict', ProvincialUserDistrictViewSet, basename='ProvincialUserEtrap')
router.register('userdistrict', DistrictUserViewSet, basename='Etrap')
router.register('userdistrictupdate', GetDistrictUserViewSet, basename='EtrapUser')
router.register('office', OfficeViewSet, basename='Office')
router.register('stafftype', StaffTypeViewSet, basename='StaffType')
router.register('staff', StaffViewSet, basename='Staff')
router.register('staffget', StaffUpdateGetViewSet, basename='Staff')
router.register('kindertar', KindergartenViewSet, basename='Kindergarten')
router.register('kindertarget', KindergartenGetViewSet, basename='Kindergarten')
router.register('school', SchoolViewSet, basename='School')
router.register('schoolget', SchoolGetViewSet, basename='School')
router.register('primaryvocationschool', PrimaryVocationSchoolViewSet, basename='PrimaryVocationSchool')
router.register('primaryvocationschoolget', PrimaryVocationSchoolGetViewSet, basename='PrimaryVocationSchool')
router.register('secondaryprofessionalschool', SecondaryProfessionalSchoolViewSet,
                basename='SecondaryProfessionalSchool')
router.register('secondaryprofessionalschoolget', SecondaryProfessionalSchoolGetViewSet,
                basename='SecondaryProfessionalSchool')
router.register('highereducationinstitute', HigherEducationInstituteViewSet, basename='SecondaryProfessionalSchool')
router.register('highereducationinstituteget', HigherEducationInstituteGetViewSet,
                basename='SecondaryProfessionalSchool')
router.register('directoruser', DirectorUserViewSet, basename='Director')
router.register('directoruserget', GetDirectorUserViewSet, basename='Director')
router.register('kindergartenlearner', KindergartenLearnerViewSet, basename='KindergartenLearner')
router.register('kindergartenlearnerget', KindergartenLearnerGetViewSet, basename='KindergartenLearner')
router.register('groupclass', GroupClassViewSet, basename='GroupClass')
router.register('groupclassget',GetGroupGroupViewSet, basename='GtoupStaff')
router.register('groupstaff', GroupStaffListViewSet, basename='GtoupStaff')
router.register('schoollearner', SchoolLearnerViewSet, basename='SchoolLearner')
router.register('schoollearnerget', SchoolLearnerGetViewSet, basename='SchoolLearner')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('AdminHome/',AdminHome),
    path('nationality/', Nationality),
    path('login/', LoginView.as_view()),
    path('forgotpassword/', ResetPasswordEmail.as_view()),
    path('emailpassword/', ResetUserPassword.as_view()),
    path('newpassword/', NewPassword.as_view()),
    path('api/', include(router.urls)),
]
handler404 = 'api.views.Error_Page_404'
handler500 = 'api.views.Error_Page_500'
